#include <stdio.h>

void getIdOfKthKilledPerson(const unsigned long long n, long long start, const unsigned long long step, const unsigned long long k, unsigned long long movedValues[], unsigned long long &response){
    if(n==1){
        if(start>0)
            response=start;
        else
            response=movedValues[0];
    }
    else{
        unsigned long long toDelete=(n>>1);
        if(toDelete<k)
            if(n&1){
                movedValues[1]=movedValues[0];
                movedValues[0]=start+(n-1)*step;
                getIdOfKthKilledPerson(n-toDelete, start-(step<<1), step<<1, k-toDelete, movedValues, response);
            }
            else
                getIdOfKthKilledPerson(n-toDelete, start, step<<1, k-toDelete, movedValues, response);
        else{
            start=start+((k<<1)-1)*step;
            if(start>0)
                response=start;
            else
                response=movedValues[1];
        }
    }
}

int main(){
    unsigned long long movedValues[2];
    unsigned long long n, p, response;
    long long k;
    scanf("%llu",&p);
    while(p--){
        scanf("%llu%lli",&n,&k);
        if(k<0)
            k=n+1+k;
        getIdOfKthKilledPerson(n, 1, 1, k, movedValues, response);
        printf("%llu\n",response);
    }
    return 0;
}

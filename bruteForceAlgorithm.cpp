#include <stdio.h>
#include <list>

inline void nextElement(std::list<long long> &list, std::list<long long>::iterator &element){
    ++element;
    if(element==list.end())
        element=list.begin();
}

inline long long getIdOfKthKilledPerson(long long n, long long k){
    std::list<long long> circle;
    for(long long i=1;i<=n;i++)
        circle.push_back(i);
    
    long long lastDeletedElement;
    std::list<long long>::iterator element=circle.begin(), next;
    while(k--){
        //get next element
        next=element;
        nextElement(circle, next);
        
        //deleting element
        lastDeletedElement=*next;
        circle.erase(next);
        
        //go to next element
        nextElement(circle, element);
    }
    return lastDeletedElement;
}

int main(){
    long long n,k,p;
    scanf("%lli",&p);
    while(p--){
        scanf("%lli%lli",&n,&k);
        if(k<0)
            k=n+1+k;
        long long response=getIdOfKthKilledPerson(n,k);
        printf("%lli\n",response);
    }
    return 0;
}
